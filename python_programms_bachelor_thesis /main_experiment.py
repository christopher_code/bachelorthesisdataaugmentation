import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
import warnings
warnings.filterwarnings('ignore')

from results.training_block import training_block
from data_augmentation.main_augmentation import main_augmentation

# loop for different datasets 

data_path =[['cardio_49.csv'],
            ['cardio_44.csv'],
            ['cardio_39.csv'],
            ['cardio_34.csv'],
            ['cardio_29.csv'],
            ['cardio_24.csv'],
            ['cardio_19.csv'],
            ['cardio_14.csv'],
            ['cardio_9.csv'],
            ['cardio_4.csv'],
            ['cardio_1.csv'],
            ['cardio_01.csv']
            ]

balance_ratio = [['49'], 
                ['44'],
                ['39'],
                ['34'],
                ['29'],
                ['24'],
                ['19'],
                ['14'],
                ['09'],
                ['04'],
                ['01'],
                ['00.1']
                ]

column_values = ['train_acc', 'train_f1', 'train_auc', 'test_acc', 'test_f1', 'test_auc'] 
test = np.array([[0,0,0,0,0,0]])
index_values = ['0']

df_structure = pd.DataFrame(
    data = test,
    index = index_values,
    columns = column_values) 

df_structure = df_structure.drop('0')

i = len(data_path)

while i > 0:
    df = pd.read_csv("./Data/" + data_path[i-1][0])

    X_train, X_test, y_train, y_test = train_test_split(df.drop(columns=['cardio']), df['cardio'], test_size = 0.2, stratify = df['cardio'], random_state = 42)

    # training with raw data 

    df_structure = training_block(X_train, y_train, X_test, y_test, df_structure, "raw", balance_ratio[i-1][0])

    # data augmentation 

    df_structure = main_augmentation(X_train, y_train, X_test, y_test, df_structure, balance_ratio[i-1][0])

    print("Results until df with balance ratio of: " + balance_ratio[i-1][0] + "%")
    print(df_structure)
    i -= 1

df_structure.to_csv('results_round_3.csv', encoding='utf-8')
