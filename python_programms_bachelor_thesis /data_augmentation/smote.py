from imblearn.over_sampling import SMOTE
import pandas as pd
import warnings
warnings.filterwarnings('ignore')

def smote(df_train): 
    smote = SMOTE(random_state = 42)
    X, y = smote.fit_resample(df_train[['id','age','gender','height','weight','ap_hi','ap_lo','cholesterol','gluc','smoke','alco','active']], df_train['cardio'])
    #Creating a new Oversampling Data Frame
    df_oversampler = pd.DataFrame(X, columns = ['id','age','gender','height','weight','ap_hi','ap_lo','cholesterol','gluc','smoke','alco','active'])
    df_oversampler_y = pd.DataFrame(y, columns = ['cardio'])

    df_smote = df_oversampler
    df_smote['cardio'] = df_oversampler_y
    print("Done with SMOTE!")

    return df_smote