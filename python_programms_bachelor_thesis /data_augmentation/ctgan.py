from ctgan import CTGANSynthesizer
import pandas as pd
import warnings
warnings.filterwarnings('ignore')

def ctgan(df_train):
    ctgan = CTGANSynthesizer()
    samples_0 = df_train[df_train['cardio'] == 0]
    samples_1 = df_train[df_train['cardio'] == 1]

    while len(samples_0) > len(samples_1): 
        if len(samples_1) <= 15000:
            samples_0_train = samples_0.sample(n = len(samples_1))
            samples_1_train = samples_1
            train_df = samples_1_train.append(samples_0_train)
        elif len(samples_1) > 15000: 
            samples_0_train = samples_0.sample(n = 15000)
            samples_1_train = samples_1.sample(n = 12000)
            train_df = samples_1_train.append(samples_0_train)
        elif len(samples_1) > 20000: 
            samples_0_train = samples_0.sample(n = 22000)
            samples_1_train = samples_1.sample(n = 18000)
            train_df = samples_1_train.append(samples_0_train)
        ctgan.fit(train_df, discrete_columns=('gender','cholesterol','gluc','smoke','alco','active','cardio'))
        samples = ctgan.sample(round(len(samples_1_train) * 1.2))
        samples = samples[samples['cardio'] ==1.0]
        samples_1 = samples_1.append(samples)
        print("Created samples (CTGAN):")
        print(len(samples))
    
    if len(samples_0) < len(samples_1):
            samples_1_balanced = samples_1.sample(n = len(samples_0))
            df_ctgan = samples_1_balanced.append(samples_0)
            print(len(samples_0))
            print(len(samples_1_balanced))
            print("done with CTGAN")

    return df_ctgan