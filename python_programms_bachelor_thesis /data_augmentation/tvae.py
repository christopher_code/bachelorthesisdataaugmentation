from sdv.tabular import TVAE
import pandas as pd
import warnings
warnings.filterwarnings('ignore')

def tvae(df_train):
    tvae = TVAE()
    samples_0 = df_train[df_train['cardio'] == 0]
    samples_1 = df_train[df_train['cardio'] == 1]

    while len(samples_0) > len(samples_1): 
        if len(samples_1) <= 15000:
            samples_0_train = samples_0.sample(n = len(samples_1))
            samples_1_train = samples_1
            # print(len(samples_1_train))
            # print(len(samples_0_train))
            train_df = samples_1_train.append(samples_0_train)
        elif len(samples_1) > 15000: 
            samples_0_train = samples_0.sample(n = 15000)
            samples_1_train = samples_1.sample(n = 12000)
            train_df = samples_1_train.append(samples_0_train)
        elif len(samples_1) > 20000: 
            samples_0_train = samples_0.sample(n = 22000)
            samples_1_train = samples_1.sample(n = 18000)
            train_df = samples_1_train.append(samples_0_train)
        tvae.fit(train_df)
        samples = tvae.sample(round(len(samples_1_train) * 1.2))
        samples = samples[samples['cardio'] ==1.0]
        samples_1 = samples_1.append(samples)
        print("Created samples (TVAE):")
        print(len(samples))
    
    if len(samples_0) < len(samples_1):
            samples_1_balanced = samples_1.sample(n = len(samples_0))
            df_tvae = samples_1_balanced.append(samples_0)
            print(len(samples_0))
            print(len(samples_1_balanced))
            print("Done with TVAE!")

    return df_tvae