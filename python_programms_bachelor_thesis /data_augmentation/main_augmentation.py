from results.training_block import training_block
from data_augmentation.smote import smote
from data_augmentation.tvae import tvae
from data_augmentation.ctgan import ctgan
import warnings
warnings.filterwarnings('ignore')

def main_augmentation(X_train, y_train, X_test, y_test, df_structure, br):
    df_train = X_train
    df_train['cardio'] = y_train

    # SMOTE 
    df_smote = smote(df_train)
    X_train_SMOTE = df_smote.drop(columns=['cardio'])
    y_train_SMOTE = df_smote['cardio']

    # train with SMOTE data 
    print("Start with smote")
    df_structure = training_block(X_train_SMOTE, y_train_SMOTE, X_test, y_test, df_structure, "smote", br)

    # TVAE 
    df_tvae = tvae(df_train)
    X_train_TVAE = df_tvae.drop(columns=['cardio'])
    y_train_TVAE = df_tvae['cardio']

    # train with TVAE data 
    print("Start with tvae")
    df_structure = training_block(X_train_TVAE, y_train_TVAE, X_test, y_test, df_structure, "tvae", br)

    # CTGAN 

    df_ctgan = ctgan(df_train)
    X_train_CTGAN = df_ctgan.drop(columns=['cardio'])
    y_train_CTGAN = df_ctgan['cardio']

    # train with TVAE data 
    print("Start with ctgan")
    df_structure = training_block(X_train_CTGAN, y_train_CTGAN, X_test, y_test, df_structure, "ctgan", br)

    return df_structure