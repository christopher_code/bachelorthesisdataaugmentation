from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report
import warnings
warnings.filterwarnings('ignore')

def log_reg(X_train, y_train, X_test, y_test): 
    classifier = LogisticRegression()
    classifier.fit(X_train, y_train)
    # print(classification_report(y_train, classifier.predict(X_train)))
    # print(classification_report(y_test, classifier.predict(X_test)))
    print("Logistic Regression done!")
    return classifier