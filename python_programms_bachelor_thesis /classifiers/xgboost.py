import xgboost as xgb
import pickle
from sklearn.metrics import classification_report
import warnings
warnings.filterwarnings('ignore')

def xgboost(X_train, y_train, X_test, y_test): 
    model = xgb.XGBClassifier(max_depth=4,
                        subsample=0.9,
                        objective='binary:logistic',
                        n_estimators=200,
                        learning_rate = 0.1)
    eval_set = [(X_train, y_train), (X_test, y_test)]
    model.fit(X_train, y_train.values.ravel(), early_stopping_rounds=10, eval_metric=["error", "logloss"], eval_set=eval_set, verbose=True)

    print("XGBoost done!")

    return model