from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.svm import SVC
import warnings
warnings.filterwarnings('ignore')

def svm(X_train, y_train, X_test, y_test): 
    clf = make_pipeline(StandardScaler(), SVC(probability=True))
    clf.fit(X_train, y_train)
    print("SVM done!")
    return clf