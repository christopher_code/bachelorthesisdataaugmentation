from classifiers.logirstic_regression import log_reg
from classifiers.svm import svm
from classifiers.xgboost import xgboost
from results.results import results
import warnings
warnings.filterwarnings('ignore')

def training_block(X_train, y_train, X_test, y_test, results_array, data, balance_ratio): 
    #logistic regression
    classifier = log_reg(X_train, y_train, X_test, y_test)
    results_array = results(X_train, y_train, X_test, y_test, classifier, classifier, results_array, data + "_lr_" + balance_ratio)

    #svm
    clf = svm(X_train, y_train, X_test, y_test)
    results_array = results(X_train, y_train, X_test, y_test, clf, clf, results_array, data + "_svm_" + balance_ratio)

    # XGBoost block 
    model = xgboost(X_train, y_train, X_test, y_test)
    results_array = results(X_train, y_train, X_test, y_test, model, model, results_array, data + "_xgb_" + balance_ratio)

    return results_array