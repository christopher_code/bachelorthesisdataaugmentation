from sklearn.metrics import classification_report
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
import pandas as pd
import warnings
warnings.filterwarnings('ignore')

def results(X_train, y_train, X_test, y_test, classification_model_train, classification_model_test, results_array, name_index): 
    train_report = classification_report(y_train, classification_model_train.predict(X_train), output_dict=True)
    train_df_classification_report = pd.DataFrame(train_report).transpose()

    ns_probs = [0 for _ in range(len(y_train))]
    lr_probs = classification_model_train.predict_proba(X_train)
    lr_probs = lr_probs[:, 1]
    ns_auc = roc_auc_score(y_train, ns_probs)
    train_lr_auc = roc_auc_score(y_train, lr_probs)

    train_df_classification_report = train_df_classification_report.drop(columns=['precision', 'recall', 'support'])
    train_df_classification_report_acc = train_df_classification_report.drop(train_df_classification_report.index[[0, 1, 3, 4]])
    train_df_classification_report_f1 = train_df_classification_report.drop(train_df_classification_report.index[[0, 1, 2, 4]])

    # test

    test_report_test = classification_report(y_test, classification_model_test.predict(X_test), output_dict=True)
    test_df_classification_report = pd.DataFrame(test_report_test).transpose()

    ns_probs = [0 for _ in range(len(y_test))]
    lr_probs = classification_model_test.predict_proba(X_test)
    lr_probs = lr_probs[:, 1]
    ns_auc = roc_auc_score(y_test, ns_probs)
    test_lr_auc = roc_auc_score(y_test, lr_probs)

    test_df_classification_report = test_df_classification_report.drop(columns=['precision', 'recall', 'support'])
    test_df_classification_report_acc = test_df_classification_report.drop(test_df_classification_report.index[[0, 1, 3, 4]])
    test_df_classification_report_f1 = test_df_classification_report.drop(test_df_classification_report.index[[0, 1, 2, 4]])

    df2 = pd.DataFrame([{'train_acc': train_df_classification_report_acc.values[0][0],
                            'train_f1': train_df_classification_report_f1.values[0][0],
                            'train_auc': train_lr_auc,
                            'test_acc': test_df_classification_report_acc.values[0][0],
                            'test_f1': test_df_classification_report_f1.values[0][0],
                            'test_auc': test_lr_auc}])

    df2 = df2.rename(index={0: name_index})

    results_array = results_array.append(df2)

    # print(results_array)
    return results_array